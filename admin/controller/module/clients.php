<?php
class ControllerModuleClients extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/clients');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('module/clients');

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('clients', $this->request->post);
			$this->model_module_clients->editImages($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$clients_image = $this->model_module_clients->getAll();
		$this->load->model('tool/image');

		foreach ($clients_image as $key => $client_image) {
			if (isset($this->request->post['image'])) {
				$image = $this->request->post['image'];
			} elseif (!empty($client_image)) {
				$image = $client_image['image'];
			} else {
				$image = '';
			}

			if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
				$clients_images = $this->model_tool_image->resize($this->request->post['image'], 120, 100);
			} elseif (!empty($clients_image) && is_file(DIR_IMAGE . $client_image['image'])) {
				$clients_images = $this->model_tool_image->resize($client_image['image'], 120, 100);
			} else {
				$clients_images = $this->model_tool_image->resize('no_image.png', 120, 100);
			}

			$image_id = $client_image['id'];
			$placeholder = $this->model_tool_image->resize('no_image.png', 120, 100);

			$data['data_images'][] = array(
				'image' => $image,
				'clients_images' => $clients_images,
				'image_id' => $image_id,
				'placeholser' => $placeholder
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/clients', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/clients', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['clients_status'])) {
			$data['clients_status'] = $this->request->post['clients_status'];
		} else {
			$data['clients_status'] = $this->config->get('clients_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/clients.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/clients')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}