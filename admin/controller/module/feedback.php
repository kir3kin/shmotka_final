<?php
class ControllerModuleFeedback extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/feedback');
		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			$this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('feedback', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->load->model('module/feedback');
		$feedback_entries = $this->model_module_feedback->getAll();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['feedback_entries'] = array();

		function get_array_data($feedback) {
			return array(
				'author' => $feedback['author'],
				'dt' => date("d.m.Y H:i:s", strtotime($feedback['timestamp'])),
				'text' => $feedback['text'],
				'photo' => $feedback['photo'],
				'id' => $feedback['id'],
				'approved' => $feedback['approved']
			);
		}

		function circle_children($childrens, $parents) {
			for($i = 0; $i < count($parents); $i++) {
				for($j = 0; $j < count($childrens); $j++) {
					if (isset($childrens[$j])) {
						if ($parents[$i]['id'] === $childrens[$j]['parent_id']) {
							$parents[$i]['children'][] = get_array_data($childrens[$j]);
						}
					}
				}
				if (isset($parents[$i]['children'])) {
					$parents[$i]['children'] = circle_children($childrens, $parents[$i]['children']);
				}
			}
			return $parents;
		}

		function add_children_data($feedback) {
			foreach ($feedback as $feedback_entry) {
				if (empty($feedback_entry['parent_id'])) {
					$data[] = get_array_data($feedback_entry);
				} else {
					$childrens[] = $feedback_entry;
				}
			}
			if (!empty($childrens)) {
				$data = circle_children($childrens, $data);
			}
			return $data;
		}

		function get_ids($childrens) {
			$ids = '';
			foreach ($childrens as $children) {
				$ids .= "," . $children['id'];
				if (isset($children['children'])) {
					$ids .= get_ids($children['children']);
				}
			}
			return $ids;
		}

		function get_need_ids($data) {
			for ($i = 0; $i < count($data); $i++) {
				$data[$i]['ids'] = $data[$i]['id'];
				if (isset($data[$i]['children'])) {
					$data[$i]['ids'] .= get_ids($data[$i]['children']);
				}
				if (isset($data[$i]['children'])) {
					$data[$i]['children'] = get_need_ids($data[$i]['children']);
				}
			}
			return $data;
		}

		if (!empty($feedback_entries)) {
			$data['feedback_entries'] = add_children_data($feedback_entries);
			$data['feedback_entries'] = get_need_ids($data['feedback_entries']);
		}

		$data['text_edit']    = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_content_top'] = $this->language->get('text_content_top');
		$data['text_content_bottom'] = $this->language->get('text_content_bottom');
		$data['text_column_left'] = $this->language->get('text_column_left');
		$data['text_column_right'] = $this->language->get('text_column_right');

		$data['entry_layout'] = $this->language->get('entry_layout');
		$data['entry_position'] = $this->language->get('entry_position');
		$data['entry_dt'] = $this->language->get('entry_dt');
		$data['entry_author'] = $this->language->get('entry_author');
		$data['entry_text'] = $this->language->get('entry_text');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_add_module'] = $this->language->get('button_add_module');
		$data['button_update'] = $this->language->get('button_update');
		$data['button_remove'] = $this->language->get('button_remove');

		$data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : '';
		$data['error_code'] = isset($this->error['code']) ? $this->error['code'] : '';

		$data['breadcrumbs'] = array(
			array(
				'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => false
			),

			array(
				'text'      => $this->language->get('text_module'),
				'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			),

			array(
				'text'      => $this->language->get('heading_title'),
				'href'      => $this->url->link('module/feedback', 'token=' . $this->session->data['token'], 'SSL'),
				'separator' => ' :: '
			)
		);

		$data['action'] = $this->url->link('module/feedback', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['feedback_status'])) {
			$data['feedback_status'] = $this->request->post['feedback_status'];
		} else {
			$data['feedback_status'] = $this->config->get('feedback_status');
		}

		$data['token'] = $this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/feedback.tpl', $data));
	}

	public function change() {
		$entry_id = $this->request->post['entry_id'];

		if ($this->user->hasPermission('modify', 'module/feedback')) {
			$this->load->model('module/feedback');

			if (isset($this->request->post['entry_dt'])) {
				$entry_dt = $this->request->post['entry_dt'];
				call_user_func(array($this->model_module_feedback, $this->request->post['action']), $entry_id, $entry_dt);
			}

			else {
				call_user_func(array($this->model_module_feedback, $this->request->post['action']), $entry_id);
			}

			$json = array('success' => '1', 'entry_id' => $entry_id);
		}

		else {
			$this->load->language('module/feedback');
			$json = array('success' => '0', 'error' => $this->language->get('error_permission'), 'entry_id' => $entry_id);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/feedback')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) return true; else return false;
	}
}
