<?php
class ControllerModuleLeedform extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/leedform');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			if (isset($this->request->post['leedform_upload'])) {
				$this->request->post['leedform_upload'] = '1';
			} else {
				$this->request->post['leedform_upload'] = '0';
			}

			if (isset($this->request->post['leedform_pc'])) {
				$this->request->post['leedform_pc'] = '1';
			} else {
				$this->request->post['leedform_pc'] = '0';
			}

			if (isset($this->request->post['leedform_mob'])) {
				$this->request->post['leedform_mob'] = '1';
			} else {
				$this->request->post['leedform_mob'] = '0';
			}

            $this->model_setting_setting->editSetting('leedform', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit']    = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');

        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['code'])) {
            $data['error_code'] = $this->error['code'];
        } else {
            $data['error_code'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/leedform', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('module/leedform', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['leedform_header'])) {
            $data['leedform_header'] = $this->request->post['leedform_header'];
        } else {
            $data['leedform_header'] = $this->config->get('leedform_header');
        }

        if (isset($this->request->post['leedform_description'])) {
            $data['leedform_description'] = $this->request->post['leedform_description'];
        } else {
            $data['leedform_description'] = $this->config->get('leedform_description');
        }

		if (isset($this->request->post['leedform_upload'])) {
			$data['leedform_upload'] = '1';
		} else {
			$data['leedform_upload'] = $this->config->get('leedform_upload');
		}

		if (isset($this->request->post['leedform_upload_text'])) {
            $data['leedform_upload_text'] = $this->request->post['leedform_upload_text'];
        } else {
            $data['leedform_upload_text'] = $this->config->get('leedform_upload_text');
        }

		if (isset($this->request->post['leedform_pc'])) {
			$data['leedform_pc'] = '1';
		} else {
			$data['leedform_pc'] = $this->config->get('leedform_pc');
		}

		if (isset($this->request->post['leedform_mob'])) {
			$data['leedform_mob'] = '1';
		} else {
			$data['leedform_mob'] = $this->config->get('leedform_mob');
		}

        if (isset($this->request->post['leedform_status'])) {
            $data['leedform_status'] = $this->request->post['leedform_status'];
        } else {
            $data['leedform_status'] = $this->config->get('leedform_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/leedform.tpl', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'module/leedform')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error) return true; else return false;
    }
}
