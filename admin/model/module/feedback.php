<?php
class ModelModuleFeedback extends Model {
	public function getAll() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "feedback_entries ORDER BY timestamp DESC");
		return $query->rows;
	}

	public function approve($id) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "feedback_entries SET approved=1 WHERE id=" . $id);
	}

	public function disapprove($id) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "feedback_entries SET approved=0 WHERE id=" . $id);
	}

	public function update_date($id, $dt) {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "feedback_entries SET timestamp='" . $dt . "' WHERE id=" . $id);
	}

	public function delete($id) {
		$ids = "";
		if (count($id) === 1) {
			$ids = "'" . $id[0] . "'";
		} else {
			for ($i = 0; $i < count($id); $i++) {
				if (!empty($id[$i + 1])) {
					$ids .= "'" . $id[$i] . "',";
				} else {
					$ids .= "'" . $id[$i] . "'";
				}
			}
		}
		$query = $this->db->query("DELETE FROM " . DB_PREFIX . "feedback_entries WHERE `id` IN (" . $ids . ")");
	}
}
