<?php
class ModelModuleClients extends Model {
	public function getAll() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "our_clients ORDER BY id");
		return $query->rows;
	}

	public function editImages($data) {
		$this->db->transaction_clients_begin();
		$rollback = false;
		for ($i = 0; $i < count($data['image']); $i++) {
			$query = $this->db->query("UPDATE " . DB_PREFIX . "our_clients SET `image`='" . $data["image"][$i] . "' WHERE `id`=" . (int)$data["image_id"][$i]);
			if ($query != true) $rollback = true;
		}
		if (!$rollback) $this->db->transaction_clients_commit();
		else $this->db->transaction_clients_rollback();
	}
}
