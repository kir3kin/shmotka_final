<?php
// Heading
$_['heading_title']       = 'Reviews';

// Text
$_['text_module']         = '';
$_['text_edit']           = 'Edit Module';
$_['text_success']        = 'Success: You have modified module!';
$_['text_enabled']        = 'Enabled';
$_['text_disabled']       = 'Disabled';

$_['entry_dt']            = 'Date:';
$_['entry_author']        = 'Author:';
$_['entry_text']          = 'Text:';
$_['entry_status']        = 'Status:';

$_['button_update']        = 'Update';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module!';
$_['error_code']          = 'Required';
