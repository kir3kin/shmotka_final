<?php
// Heading
$_['heading_title']       = 'Category Sales';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module categories sales!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';

$_['text_browse']    = 'Browse';
$_['text_clear']   = 'Clear';

$_['text_price_hidden']   = 'Hide prices';
$_['text_price_max']   = 'Show MAX sale';
$_['text_price_min']   = 'Show MIN price';

// Entry
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_count']    	  = 'Product Count:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module categories sales!';
?>