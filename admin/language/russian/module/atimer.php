<?php
// Heading
$_['heading_title']       = 'Таймер акции';

// Text
$_['text_module']         = 'Модули';
$_['text_edit']           = 'Настройки модуля';
$_['text_success']        = 'Настройки успешно изменены!';
$_['text_enabled']        = 'Включено';
$_['text_disabled']       = 'Отключено';

// Entry
$_['entry_status']        = 'Статус:';
$_['entry_label']         = 'Надпись:';
$_['entry_date']          = 'Дата начала акции(год/месяц/день/час/минута):';
$_['entry_period']        = 'Период счетчика(дни/часы/минуты):';
$_['entry_lbl']           = 'надпись';
$_['entry_year']          = 'год';
$_['entry_mon']           = 'месяц';
$_['entry_day']           = 'день';
$_['entry_hour']          = 'час';
$_['entry_min']           = 'минута';

// Error
$_['error_permission']    = 'Внимание: Недостаточно полномочий для изменения модуля!';
$_['error_code']          = 'Обязательный параметр';
