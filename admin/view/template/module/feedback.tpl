<?php echo $header, $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
		<button type="submit" form="form-feedback" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>

			<h1><?php echo $heading_title; ?></h1>

			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<div class="container-fluid">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>

			<div class="panel-body">
		<div class="col-sm-10">
			<h3><?php echo $heading_title; ?></h3>

			<style>
				.entry_block { margin-top: 25px; line-height: 22px; }
				.entry_field { display: inline-block; width: 70px; font-weight: bold; }
				.entry_switcher { display: inline-block; width: 100px; height: 23px; padding: 0; }
				.entry_btn { margin-left: 5px; padding: 0; width: 95px; height: 23px; line-height: 20px; vertical-align: top; }
				.entry_dt_input { width: 130px; height: 23px; text-align: center; }
				.entry_photo { width: 50px; height: 50px; }
				.source_tree { padding-left: 30px; }
				.no-reviews { font-size: 1.2em; font-style: bold; }
			</style>

			<script type="text/javascript">
				function ajaxError(xhr, opt, err) { alert(err); }
				function ajaxSuccess(data) { if (data.success == '0') alert(data.error); }

				function updateDate(id, dt) {
					var d;

					dt = dt.split(' ');
					d = dt[0].split('.');

					dt = d[2] + '-' + d[1] + '-' + d[0] + ' ' + dt[1];

					$.ajax({
									url: 'index.php?route=module/feedback/change&token=<?php echo $token; ?>',
									type: 'post',

									dataType: 'json',
									data: { entry_id: id, entry_dt: dt, action: 'update_date' },

									success: ajaxSuccess,
						error: ajaxError
							});
				}

				function changeEntry(id, val) {
					$.ajax({
									url: 'index.php?route=module/feedback/change&token=<?php echo $token; ?>',
									type: 'post',

									dataType: 'json',
									data: { entry_id: id, action: val == '1' ? 'approve' : 'disapprove' },

									success: ajaxSuccess,
						error: ajaxError
							});
				}

				function deleteEntry(id) {
					$.ajax({
						url: 'index.php?route=module/feedback/change&token=<?php echo $token; ?>',
						type: 'post',

						dataType: 'json',
						data: { entry_id: arguments, action: 'delete' },

						success: function(data) { 
							if (data.success == '1') {
									$(data.entry_id).each(function() {
										$('#feedback_entry_' + this).remove();
									});
							} else {
								alert(data.error);
							}
					},
						error: ajaxError
					});
				}
			</script>

	<?php function get_feedbacks($feedback, $entry_dt, $entry_author, $entry_status, $entry_text, $text_disabled, $text_enabled, $button_remove, $button_update) { ?>

		<div style="padding: 0 0 15px 0;">

			<?php foreach ($feedback as $feedback_entry) { ?>

			<div id="feedback_entry_<?php echo $feedback_entry['id']; ?>" class="entry_block">
				<div>
					<div class="entry_field"><?php echo $entry_dt; ?></div>
					<input id="feedback_entry_dt_<?php echo $feedback_entry['id']; ?>" type="text" value="<?php echo $feedback_entry['dt']; ?>" class="entry_dt_input">

					<div class="btn btn-primary entry_btn" onclick="updateDate(<?php echo $feedback_entry['id']; ?>, $('#feedback_entry_dt_<?php echo $feedback_entry['id']; ?>').val());">
						<?php echo $button_update; ?>
					</div>
				</div>

				<div><div class="entry_field"><?php echo $entry_author; ?></div><?php echo $feedback_entry['author']; ?></div>

				<?php if ($feedback_entry['photo']) { ?>
					<div><div class="entry_field">Фото:</div><img class="entry_photo" src="<?php echo $feedback_entry['photo']; ?>" /></div>
				<?php } ?>

				<div><div class="entry_field"><?php echo $entry_text; ?></div><?php echo $feedback_entry['text']; ?></div>

				<div style="margin-top: 2px;">
					<div class="entry_field"><?php echo $entry_status; ?></div>

					<select class="form-control entry_switcher" onchange="changeEntry(<?php echo $feedback_entry['id']; ?>, this.value);">
						<?php if ($feedback_entry['approved']) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					</select>

					<div class="btn btn-danger entry_btn" onclick="deleteEntry(<?php echo $feedback_entry['ids']; ?>);"><?php echo $button_remove; ?></div>
				</div>
			</div>

		<div class="source_tree">
				<?php if (!empty($feedback_entry['children'])) { ?>
					<?php get_feedbacks($feedback_entry['children'], $entry_dt, $entry_author, $entry_status, $entry_text, $text_disabled, $text_enabled, $button_remove, $button_update); ?>
				<?php } ?>
		</div>
			<?php } ?>

		</div>

	<?php } ?>


	<?php if (!empty($feedback_entries)) {
		get_feedbacks($feedback_entries, $entry_dt, $entry_author, $entry_status, $entry_text, $text_disabled, $text_enabled, $button_remove, $button_update);
	} else { ?>
	<div class="no-reviews">Нет текущих отзывов!</div>
	<?php } ?>



		</div>

		<div class="col-sm-10">
			<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-feedback" class="form-horizontal">
				<select name="feedback_status" id="input-status" class="form-control" style="margin-top: 15px;">
					<?php if ($feedback_status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				</select>
			</form>
		</div>
			</div>
	</div>
	</div>
</div>
<?php echo $footer; ?>
