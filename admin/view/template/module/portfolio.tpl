<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-portfolio" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<style>
			.img_block { margin-bottom: 30px; }
			.img_el { width: 55%; margin-bottom: 10px; }
			.img_order_lbl { font-weight: bold; vertical-align: middle; }
			.img_order_ti { width: 33px; height: 23px; margin-right: 10px; text-align: right; }
			.img_btn { width: 95px; height: 23px; margin-left: 5px; line-height: 20px; padding: 0; vertical-align: middle; }
			.st_div { margin-top: 50px; }
		</style>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-portfolio" class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-2 control-label">Текст описания:</label>

	            <div class="col-sm-10">
					<textarea class="form-control" name="portfolio_description" rows="5"><?php echo $portfolio_description; ?></textarea>
	            </div>
	        </div>

	        <div class="form-group">
	            <label class="col-sm-2 control-label">Изображения:</label>

	            <div id="portfolio_images" class="col-sm-10">
					<?php foreach ($portfolio_images as $portfolio_image) { ?>
						<div id="img_block_<?php echo $portfolio_image['id']; ?>" class="img_block">
							<img id="img_<?php echo $portfolio_image['id']; ?>" class="img_el" src="<?php echo $portfolio_image['path']; ?>">

							<div>
								<span class="img_order_lbl">Порядок:&nbsp;</span>
								<input id="img_order_<?php echo $portfolio_image['id']; ?>" type="text" class="img_order_ti" maxlength="4" value="<?php echo $portfolio_image['sort_order']; ?>">

								<div class="btn btn-primary img_btn" onclick="changeImage(<?php echo $portfolio_image['id']; ?>);">Изменить</div>
								<div class="btn btn-danger img_btn" onclick="deleteImage(<?php echo $portfolio_image['id']; ?>);">Удалить</div>
							</div>
						</div>
					<?php } ?>

					<div id="img_add_btn" class="btn btn-primary">Добавить изображение</div>
	            </div>
	        </div>

	        <div class="form-group st_div">
	            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
	            <div class="col-sm-10">
	              <select name="portfolio_status" id="input-status" class="form-control">
	                <?php if ($portfolio_status) { ?>
		                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
		                <option value="0"><?php echo $text_disabled; ?></option>
	                <?php } else { ?>
		                <option value="1"><?php echo $text_enabled; ?></option>
		                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
	                <?php } ?>
	              </select>
	            </div>
	        </div>
        </form>

        <?php require(DIR_TEMPLATE.'tool/imageupload.tpl'); ?>

		<script type="text/javascript">
			function addImgBlock(id, path, order) {
				$('<div id="img_block_' + id + '" class="img_block">' +
					'<img id="img_' + id + '" class="img_el" src="' + path + '">' +

					'<div>' +
					   '<span class="img_order_lbl">Порядок:&nbsp;</span>' +
						'<input id="img_order_' + id + '" type="text" class="img_order_ti" maxlength="4" value="' + order + '">'+

						'<div class="btn btn-primary img_btn" onclick="changeImage(' + id + ');">Изменить</div>' +
						'<div class="btn btn-danger img_btn" onclick="deleteImage(' + id + ');">Удалить</div>' +
					'</div>' +
				  '</div>').insertBefore(document.getElementById('img_add_btn'));
			}

			function ajaxError(xhr, opt, err) { alert(err); }

			function uploadImage(data) {
				if (data.error) alert(data.error);
				if (data.success) addImage(data.img_url);
			}

			function addImage(path) {
				$.ajax({
		            url: 'index.php?route=module/portfolio/change&token=' + getURLVar('token'),
		            type: 'post',

		            dataType: 'json',
		            data: { action: 'add', image_id: 'new', path: path, sort_order: '0' },

		            success: function(data) { if (data.success == '1') addImgBlock(data.image_id, path, '0'); else alert(data.error); },
					error: ajaxError
		        });
			}

			function changeImage(id) {
				var path = $('#img_' + id).attr('src'),
					order = $('#img_order_' + id).val();

				$.ajax({
		            url: 'index.php?route=module/portfolio/change&token=' + getURLVar('token'),
		            type: 'post',

		            dataType: 'json',
		            data: { action: 'update', image_id: id, path: path, sort_order: order },

		            success: function(data) { if (data.success == '0') alert(data.error); },
					error: ajaxError
		        });
			}

			function deleteImage(id) {
				$.ajax({
		            url: 'index.php?route=module/portfolio/change&token=' + getURLVar('token'),
		            type: 'post',

		            dataType: 'json',
		            data: { action: 'delete', image_id: id },

		            success: function(data) { if (data.success == '1') $('#img_block_' + data.image_id).remove(); else alert(data.error); },
					error: ajaxError
		        });
			}

			$('.img_order_ti').keypress(testNumberInput);
			addUploadHandler(document.getElementById('img_add_btn'), uploadImage);
		</script>
      </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
