<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-atimer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<style>
			.lb { width: 350px; }
			.lb_ti { width: 350px;}
			.dt_div { width: 180px; margin-top: 6px; }
			.yy_ti { width: 35px; text-align: center; }
			.dt_ti { width: 25px; text-align: center; }
			.st_div { margin-top: 50px; }
		</style>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-atimer" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label lb" for="textarea-atimer_text_field"><?php echo $entry_label; ?></label>

            <div class="col-sm-10 dt_div">
              <input class="lb_ti" type="text" name="atimer_label_text" value="<?php echo $atimer_label_text; ?>" placeholder="<?php echo $entry_lbl; ?>">

              <?php if ($error_code) { ?>
	              <div class="text-danger"><?php echo $error_code; ?></div>
              <?php } ?>
            </div>
        </div>

          <div class="form-group">
            <label class="col-sm-2 control-label lb" for="textarea-atimer_text_field"><?php echo $entry_date; ?></label>

            <div class="col-sm-10 dt_div">
              <input class="yy_ti" type="text" name="atimer_year" value="<?php echo $atimer_year; ?>" placeholder="<?php echo $entry_year; ?>">
              <input class="dt_ti" type="text" name="atimer_mon" value="<?php echo $atimer_mon; ?>" placeholder="<?php echo $entry_mon; ?>">
              <input class="dt_ti" type="text" name="atimer_day" value="<?php echo $atimer_day; ?>" placeholder="<?php echo $entry_day; ?>">
              <input class="dt_ti" type="text" name="atimer_hour" value="<?php echo $atimer_hour; ?>" placeholder="<?php echo $entry_hour; ?>">
              <input class="dt_ti" type="text" name="atimer_min" value="<?php echo $atimer_min; ?>" placeholder="<?php echo $entry_min; ?>">

              <?php if ($error_code) { ?>
	              <div class="text-danger"><?php echo $error_code; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label lb" for="textarea-atimer_text_field"><?php echo $entry_period; ?></label>

            <div class="col-sm-10 dt_div">
              <input class="dt_ti" type="text" name="atimer_period_day" value="<?php echo $atimer_period_day; ?>" placeholder="<?php echo $entry_day; ?>">
              <input class="dt_ti" type="text" name="atimer_period_hour" value="<?php echo $atimer_period_hour; ?>" placeholder="<?php echo $entry_hour; ?>">
              <input class="dt_ti" type="text" name="atimer_period_min" value="<?php echo $atimer_period_min; ?>" placeholder="<?php echo $entry_min; ?>">

              <?php if ($error_code) { ?>
	              <div class="text-danger"><?php echo $error_code; ?></div>
              <?php } ?>
            </div>
          </div>

          <div class="form-group st_div">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="atimer_status" id="input-status" class="form-control">
                <?php if ($atimer_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
