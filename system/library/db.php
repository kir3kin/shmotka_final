<?php
class DB {
	private $db;

	public function __construct($driver, $hostname, $username, $password, $database) {
		$class = 'DB\\' . $driver;

		if (class_exists($class)) {
			$this->db = new $class($hostname, $username, $password, $database);
		} else {
			exit('Error: Could not load database driver ' . $driver . '!');
		}
	}

	public function query($sql) {
		return $this->db->query($sql);
	}

	public function escape($value) {
		return $this->db->escape($value);
	}

	public function countAffected() {
		return $this->db->countAffected();
	}

	public function transaction_clients_begin() {
		return $this->db->transaction_clients_begin();
	}

	public function transaction_clients_commit() {
		return $this->db->transaction_clients_commit();
	}

	public function transaction_clients_rollback() {
		return $this->db->transaction_clients_rollback();
	}

	public function getLastId() {
		return $this->db->getLastId();
	}
}
