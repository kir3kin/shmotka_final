	(function($) {
	$(document).ready(function() {
		if ($(window).width() < 768) {
			// var currentOffset = $($(".container").has("#menu")[1]).offset().top - 20,
					var currentOffset = $("#menu").offset().top - 20,
					stateUP = false,
					stateDOWN = false,
					delayUP = function() {
						if (stateUP) return true;
						stateUP = true;
						setTimeout(function() { stateUP = false }, 600);
						return false;
					},
					delayDOWN = function() {
						if (stateDOWN) return true;
						stateDOWN = true;
						setTimeout(function() { stateDOWN = false }, 600);
						return false;
					};

			$(document).scroll(function() {
				if ($(window).scrollTop() > currentOffset) {
					if (delayUP()) return;
					$(".container").has("#menu").css({
						position: "fixed",
						top: "40px",
						zIndex: "96"
					});
					$(".breadcrumb")? $(".breadcrumb").css("margin-top", "65px"): "";
					$(".owl-carousel")? $(".owl-carousel").css("margin-top", "65px"): "";
				}
				else {
					if (delayDOWN()) return;
					$(".container").has("#menu").css({
						position: "relative",
						top: "0"
					});
					$(".breadcrumb")? $(".breadcrumb").css("margin-top", "0"): "";
					$(".owl-carousel")? $(".owl-carousel").css("margin-top", "0"): "";
				}
			});
		}

		$("a[href=#modCallMe]").click(function() {// для формирования комментария при добавлении заказа
			$("#callback_order input[name=form_name]").val($(this).attr("data-form-name"));
		});

		var topBlock = $(".unique");

		if ((topBlock.length > 0)) {
			topBlock.mouseout(function() {
				$($("a[href=#modCallMe]")[0]).trigger("click");// вызов модального окна
				topBlock.remove();
			});
		}
	});
}(jQuery));
