<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?><meta name="description" content="<?php echo $description; ?>" /><?php } ?>
	<?php if ($keywords) { ?><meta name="keywords" content= "<?php echo $keywords; ?>" /><?php } ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<?php if ($icon) { ?><link href="<?php echo $icon; ?>" rel="icon" type="image/png"/><?php } ?>
	<?php foreach ($links as $link) { ?><link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" /><?php } ?>
	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
	<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet" type="text/css" />
	<link href="catalog/view/theme/default/stylesheet/callback_btn.css" rel="stylesheet" type="text/css" />
	<script src="//vk.com/js/api/openapi.js" type="text/javascript" charset="windows-1251"></script>
	<?php foreach ($styles as $style) { ?><link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" /><?php } ?>
	<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/changes.js" type="text/javascript"></script>
	<?php foreach ($scripts as $script) { ?><script src="<?php echo $script; ?>" type="text/javascript"></script><?php } ?>
	<?php echo $google_analytics; ?>
</head>

<body class="<?php echo $class; ?>">
<div class="unique"></div> 
<nav id="top" style="position: fixed; top: 0; width: 100%; z-index: 1000;">
	<div class="container">
		<div id="top-links" class="nav pull-right top-phones">
			<ul class="list-inline" style="margin-bottom: 0;">
		<li><a href="tel:<?php echo $phone_1; ?>"><i class="fa fa-phone"></i> <span><?php echo $phone_1; ?></span></a></li>
		<li><a href="tel:<?php echo $phone_2; ?>"><i class="fa fa-phone"></i> <span><?php echo $phone_2; ?></span></a></li>

				<li class="hidden-xs"><a href="#modCallMe" data-form-name="Обратный звонок" data-toggle="modal"><i class="fa fa-phone"></i> <span class="hidden-xs">Обратный звонок</span></a></li>
				<li class="hidden-xs"><a href="<?php echo $contact; ?>"><i class="fa fa-envelope"></i> <span class="hidden-xs hidden-sm">Напишите нам</span></a></li>

				<li class="dropdown hidden-xs"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a>
					<ul class="dropdown-menu dropdown-menu-right">
						<?php if ($logged) { ?>
							<li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
							<li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
							<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
							<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
							<li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
						<?php } else { ?>
							<li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
							<li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
						<?php } ?>
					</ul>
				</li>
				<li class="hidden-xs"><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm"><?php echo $text_shopping_cart; ?></span></a></li>
				<li class="hidden-xs"><a href="<?php echo HTTP_SERVER; ?>index.php?route=checkout/buy#checkout-f" title="<?php echo $text_checkout; ?>"><i class="fa fa-share"></i> <span class="hidden-xs hidden-sm"><?php echo $text_checkout; ?></span></a></li>
			</ul>
		</div>
	</div>
</nav>

<div class="modal fade" id="modCallMe" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="text-align: left;">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="myModalLabel">Обратный звонок</h3>
			</div>

			<style>
				@media (max-width: 478px) {
					.mob_hide_topic { display: none; }
				}
			</style>

			<div class="modal-body" style="text-align: left;">
				<?php
					if(isset($_POST["call_phone"])) {
						if($_POST["call_name"] & $_POST["call_phone"]) {
							setcookie("callback", "ok", time()+60);

							// $mail = new Mail();
							// $mail->setTo($_POST["call_email"]);
							// $mail->setFrom($_POST["call_email"]);
							// $mail->setSender('ОБРАТНЫЙ ЗВОНОК - SHMOTKA.COM.UA');
							// $mail->setSubject('перезвоните мне: '.htmlspecialchars($_POST["call_phone"]));
							// $mail->setHtml('Телефон клиента: <b>'.htmlspecialchars($_POST["call_phone"]).'<br>Имя клиента: '.htmlspecialchars($_POST["call_name"]).'</b><br>Тема вопроса: '.htmlspecialchars($_POST["call_text"]).'<br><br>IP клиента: '.$_SERVER['REMOTE_ADDR']);
							// $mail->send();

							// $this->response->redirect($this->url->link('checkout/success'));
						}
					}

					if(isset($_COOKIE["callback"])) { ?>
						<div class="alert alert-success"><b>Ваша заявка принята!</b><br>Менеджер магазина перезвонит Вам в ближайшее время.</div>
				<?php } else { ?>
						<script type=""></script>
						<form id="callback_order" method="post" action="">
							<div class="clearfix" style="margin-bottom: 20px;">
								<img src="catalog/view/theme/default/image/caller.jpg" style="float: left; margin-right: 20px; width: 100px; height: 100px;">
								<p style="text-align: justify;">Возможно, Вы не получили достаточно информации пребывая на нашем сайте. Наш менеджер с радостью бесплатно проконсультирует Вас по всем вопросам. Оставьте Ваш номер и мы свяжемся с Вами!</p>
							</div>
							<input type="hidden" name="call_email" value="<?php echo $email; ?>">
							<label>Номер телефона</label>
							<input type="text" maxlength="20" name="call_phone" class="form-control" required>
							<label>Ваше имя</label>
							<input type="text" name="call_name" class="form-control" required>
							<label class="mob_hide_topic">Тема вопроса</label>
							<textarea rows="3" type="text" name="call_text" class="form-control mob_hide_topic"></textarea>
							<input type="hidden" name="form_name" value="">
							<div class="modal-footer" style="text-align: right;">
								<input type="submit" name="call_send" class="btn btn-primary" value="Заказать">
							</div>
						</form>
						<?php require(DIR_TEMPLATE.'default/template/common/add_order_callback.tpl'); ?>
				<?php } ?>
			</div>

			<script type="text/javascript">$("input[name='call_phone']").keypress(testPhoneInput);</script>
		</div>
	</div>
</div>

<header style="padding-top: 50px; margin-bottom: 7px;">
	<div class="container">
		<div class="row">
			<div class="header-new-left">
						<div id="logo" class="header-logo">
								<?php
								 if ($logo) { ?>
									<a href="<?php echo $home; ?>"><img width="100%" src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a>
								<?php } else { ?>
									<h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
								<?php } ?>
						</div>

						<div class="header-cart-mobile">
					<a href="<?php echo $shopping_cart; ?>"><img width="57%" src="image/cart.png" style="margin-right: 20px;" /></a>
					<div style="font-size: 11px;"><?php echo $cart; ?></div>
						</div>
			</div>

					<div class="header-new-right">
				<div class="header-phones-mobile">
					<img width="70%" src="image/phones.png" style="margin-right: 10px;" />
				</div>

				<div class="header-callback-mobile">
					<a href="#modCallMe" data-form-name="Обратный звонок" data-toggle="modal"><img width="55%" src="image/callme.png" style="margin-right: 20px; margin-top: 10px;" /></a>
				</div>

				<div class="header-new-right-block">
					<div class="header-new-right-block" style="position: relative; top: 10px;">
						<a href="<?php echo $shopping_cart; ?>"><img width="57%" src="image/cart.png" style="margin-right: 20px;" /></a>
						<div style="font-size: 11px;"><?php echo $cart; ?></div>
					</div>

					<div class="header-new-right-block">
						<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/contact" data-toggle="modal"><img width="50%" src="image/letter.png" /></a>
					</div>
				</div>

				<div class="header-new-right-block">
					<div class="header-new-right-block"><a href="#modCallMe" data-form-name="Обратный звонок" data-toggle="modal"><img width="50%" src="image/callme.png" /></a></div>
					<div class="header-new-right-block"><img width="65%" src="image/phones.png" /></div>
				</div>

				<div class="header-nav">
					<a href="<?php echo HTTP_SERVER; ?>">Главная</a>
					<div class="header-nav-sep"></div>
					<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/information&information_id=4">О нас</a>
					<div class="header-nav-sep"></div>
					<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/portfolio">Наши работы</a>
					<div class="header-nav-sep"></div>
					<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/information&information_id=9">Размеры и цвета</a>
					<div class="header-nav-sep"></div>
					<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/information&information_id=8">Доставка и оплата</a>
					<div class="header-nav-sep"></div>
					<a href="<?php echo HTTP_SERVER; ?>index.php?route=information/contact">Контакты</a>
				</div>
			</div>
			</div>
	</div>
</header>

<?php if ($categories) { ?>
	<div class="container col-xs-12 hidden-sm hidden-md hidden-lg">
		<nav id="menu" class="navbar">
			<div class="navbar-header"><span id="category" class="visible-xs"><a style="color: #fff;" href="<?php echo HTTP_SERVER; ?>index.php?route=product/category&path=90">КАТАЛОГ</a></span>
				<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<?php foreach ($categories as $category) if ($category['top']) { ?>
						<?php if ($category['children']) { ?>
						<li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
							<div class="dropdown-menu">
								<div class="dropdown-inner">
									<?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
										<ul class="list-unstyled">
											<?php foreach ($children as $child) { ?>
												<li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
											<?php } ?>
										</ul>
									<?php } ?>
						</li>
						<?php } else { ?>
							<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
						<?php } ?>
					<?php } ?>
				</ul>
			</div>
		</nav>
	</div>
<?php } ?>

<div class="container hidden-xs">
	<nav id="menu" class="navbar nav_menu">
			<div class="navbar-header">
			<span id="category" class="visible-xs"><a style="color: #fff;" href="<?php echo HTTP_SERVER; ?>index.php?route=product/category&path=90">КАТАЛОГ</a></span>
			<button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
			</div>

			<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><a href="<?php echo HTTP_SERVER; ?>"><i class="fa fa-home"></i></a></li>

				<?php foreach ($categories as $category) if ($category['top_pc']) { ?>
					<li class="nav_item">
						<a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>

						<div class="nav_div">
							<?php
								$max = $n = 6;
								$html = '<ul>';

								foreach ($category['children'] as $child) {
									if ($n == 0) {
										$html .= '<ul>';
										$n = $max;
									}

									$html .= '<li><a href="' . $child['href'] . '">' . $child['name'] . '</a></li>';
									if (--$n == 0) $html .= '</ul>';
								}

								if ($n > 0) $html .= '</ul>';
								echo $html;
							?>
						</div>
					</li>
				<?php } ?>
			</ul>
			</div>
	</nav>
</div>