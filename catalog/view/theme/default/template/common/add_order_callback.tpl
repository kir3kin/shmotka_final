<script type="text/javascript">
$('#callback_order').submit(function() {

	$('input[name=call_send]').on('click', function() { return false; });

	var order_info = {
		product_id: '0',
		firstname: $(this).find('input[name=call_name]').val(),
		telephone: $(this).find('input[name=call_phone]').val(),
		comment: $(this).find('input[name=form_name]').val(),
		call_text: $(this).find('textarea[name=call_text]').val(),
		address: '---',
		send: "callback"
	};

	$.ajax({
		url: 'index.php?route=checkout/buy/buy_now',
		type: 'post',
		dataType: 'json',
		data: order_info,
		success: function(data) {
			if (data.success == '1') location = 'index.php?route=checkout/success';
			else console.warn(data);
		},
		error: function(xhr, opt, err) {
			console.error(err);
		}
	});

	// var order_send = {
	// 	call_email: $(this).find('input[name=call_email]').val(),
	// 	call_phone: $(this).find('input[name=call_phone]').val(),
	// 	call_name: $(this).find('input[name=call_name]').val(),
	// 	call_text: ($(this).find('textarea[name=call_text]').val().length !== 0) ? $(this).find('textarea[name=call_text]').val() : '---'
	// };

	// $.ajax({
	// 	url: '',
	// 	type: 'post',
	// 	dataType: 'html',
	// 	data: order_send,
	// 	success: function() {
	// 		location = 'index.php?route=checkout/success';
	// 	},
	// 	error: function(xhr, opt, err) {
	// 		console.error(err);
	// 	}
	// });

	return false;
});
</script>