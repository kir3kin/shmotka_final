<script type="text/javascript">
	$('#leedform_order_btn').click(function() {
		var order_info = {
			product_id: '0',
			firstname: $(this).parent().find("input[name='leedform_name']")[0].value,
			telephone: $(this).parent().find("input[name='leedform_phone']")[0].value,
			order_image: document.getElementById('upload_image_url').value,
			address: '---',
			comment: $(this).parent().find("input[name=form_name]")[0].value
		};

		if (order_info.firstname == '' || order_info.firstname.length > 32) { alert('Имя должно быть от 1 до 32 символов!'); return; }
		if (order_info.telephone == '') { alert('Номер телефона не соответствует заданному шаблону!'); return; }
		if (order_info.order_image == '') delete order_info.order_image;

		$.ajax({
			url: 'index.php?route=checkout/buy/buy_now',
			type: 'post',
			dataType: 'json',
			data: order_info,

			success: function(data) {
				if (data.success == '1') location = 'index.php?route=checkout/success';
				else console.warn(data);
			},

			error: function(xhr, opt, err) {
				console.error(err);
				console.log(xhr);
			}
		});
	});
</script>