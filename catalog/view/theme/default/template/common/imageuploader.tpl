<script type="text/javascript">
	function addUploadHandler(btn, handler) {
		function uploadHandler() {
			$('#user_image_upload').remove();
			$('body').prepend('<form enctype="multipart/form-data" id="user_image_upload" style="display: none;"><input type="file" name="file" value="" /></form>');
			$('#user_image_upload input[name=\'file\']').trigger('click');

			timer = setInterval(function() {
				if ($('#user_image_upload input[name=\'file\']').val() != '') {
					clearInterval(timer);

					$.ajax({
						url: 'index.php?route=common/imageuploader/upload',
						type: 'post',
						dataType: 'json',
						data: new FormData($('#user_image_upload')[0]),
						cache: false,
						contentType: false,
						processData: false,

						beforeSend: function() {
							btn.style.display = 'none';
							$('<i id="upload_button_replacer" class="fa fa-circle-o-notch fa-spin"></i>').insertAfter(btn);
						},

						complete: function() {
							btn.style.display = '';
							$('#upload_button_replacer').remove();
						},

						success: function(json) {
							if (handler) handler(json); else console.log(json);
						},

						error: function(xhr, opt, err) {
							alert(err + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
						}
					});
				}
			}, 500);
		}

		$(btn).on('click', uploadHandler);
	}
</script>
