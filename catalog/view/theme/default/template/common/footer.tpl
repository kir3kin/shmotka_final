<footer>
	<div id="callback-btn">
		<a title="Обратный звонок" data-form-name="Мигающий звонок" href="#modCallMe" data-toggle="modal">
				<div class="callback-btn-circle"></div>
				<div class="callback-btn-circle-fill"></div>
				<div class="callback-btn-img-circle"></div>
		</a>
	</div>

	<div id="goup-btn"></div>

	<div class="container">
		<div class="row">
		<div style="text-align: center" class="col-xs-12 hidden-sm hidden-md hidden-lg">
				<ul class="list-unstyled">
					<?php foreach ($informations as $information) { ?>
					<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
					<?php } ?>
					<li><a href="<?php echo $contact; ?>">Контакты</a></li>
				</ul>
		<br><br>
		</div>
		<div style="text-align: center; height: 50px" class="col-xs-12">
		<?php echo "2014 - " . date('Y') . " &copy Интернет-магазин стильной одежды - <a href=''>shmotka.com.ua</a>"; ?>
		</div>
	<div style="text-align: right" class="col-md-2 col-sm-2 col-xs-6"><?php echo $retargeting_yandex; ?></div>
	<div style="text-align: right" class="col-md-2 col-sm-2 col-xs-6">
		<?php echo $route == 'checkout-success' ? $retargeting_vk_success : $retargeting_vk; ?>
	</div>
	</div>
</footer>
<script type="text/javascript">
	var mobile_menu_expanded = <?php echo $mobile_menu_expanded; ?>;

	$(document).ready(function() {
		var bodyClass = $('body')[0].className,
			docHeight = window.innerHeight,
			docWidth = $(document).width(),
			isMob = docWidth < 768;
		if (bodyClass == 'checkout-cart' || bodyClass.indexOf('product-category') != -1 || bodyClass.indexOf('product-product') != -1) {
			var y = $("#content").offset().top - 80;
			if (isMob) $('html, body').animate({ scrollTop: y }, 700);
		}

		if ((window.location.pathname === '/') || (window.location.href.indexOf('common/home') !== -1)) {
			if (mobile_menu_expanded) $('.btn-navbar').click();
		}

		$('#goup-btn').click(function() { $('html, body').animate({ scrollTop: 0 }, 300); });

		$(window).scroll(function() {
			if (isMob) {
				if ($(this).scrollTop() > 100) $('#goup-btn').show();
				else $('#goup-btn').hide();
			}
			else {
				if ($(this).scrollTop() > docHeight + docHeight / 3) $('#goup-btn').show();
				else $('#goup-btn').hide();
			}
		});

		//call back and mini cart
		var $mini_cart = $(".cartot-price");

		if ($($mini_cart[0]).text() === "0 грн.") {
			$mini_cart.parent().css("display", "none");
	
			if (document.cookie.indexOf('iWasHereEarlier') !== -1) {
				if (bodyClass == 'common-home') $("#callback-btn").show();
			} else {
				if (bodyClass == 'common-home') setTimeout(function() { $("#callback-btn").show(); }, 30000);
			}
		} else {
			$mini_cart.parent().css("display", "block");
		}

		if (window.location.pathname === "/oc_shmotki/") {
			document.cookie = "iWasHereEarlier=1";
		}

	});
</script>
</body></html>
