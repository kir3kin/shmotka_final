<script type="text/javascript">
	var name = "<?=$this->request->post['name'];?>",
			phone = "<?=$this->request->post['phone'];?>",
			comment = "<?=$this->request->post['form_name'];?>",
			enquiry = "<?=$this->request->post['enquiry'];?>";

	var xhr = new XMLHttpRequest();
	xhr.open('POST', 'index.php?route=checkout/buy/buy_now', true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send('send=contact_form&call_text=' + encodeURIComponent(enquiry) + '&product_id=0&firstname=' + encodeURIComponent(name) + '&telephone=' + encodeURIComponent(phone) + '&comment=' + encodeURIComponent('Форма "' + comment + '"'));

	xhr.onreadystatechange = function() {
		if (xhr.readyState !== 4) return;
		if (xhr.status !== 200) {
			console.error('xhr error --- ' + xhr.status + ': ' + xhr.statusText);
		} else {
			location = "index.php?route=checkout/success";
		}
	}
</script>