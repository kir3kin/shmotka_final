<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	    <h1><?php echo $heading_title; ?></h1>
		<p><?php echo $heading_info; ?></p>

		<style>
			#upload_image_img { width: 50px; height: 50px; }
		</style>

		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
			<fieldset>
				<h3><?php echo $button_submit; ?></h3>

				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>

					<div class="col-sm-10">
						<input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="form-control">

						<?php if ($error_name) { ?>
					  	<div class="text-danger"><?php echo $error_name; ?></div>
			      <?php } ?>
					</div>
				</div>

				<div id="img_block" class="form-group" style="display: none;">
					<label class="col-sm-2 control-label">Фото</label>

					<div class="col-sm-10">
						<img id="upload_image_img" src="<?php echo $photo; ?>" />
						<input id="input-photo" type="hidden" name="photo" value="<?php echo $photo; ?>">
					</div>
				</div>

				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-comment"><?php echo $entry_comment; ?></label>

					<div class="col-sm-10">
						<textarea name="comment" rows="10" id="input-comment" class="form-control"><?php echo $comment; ?></textarea>

						<?php if ($error_comment) { ?>
					        <div class="text-danger"><?php echo $error_comment; ?></div>
			            <?php } ?>
					</div>
				</div>

				<div class="form-group required">
          <label class="col-sm-2 control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>

          <div class="col-sm-10">
	          <input type="text" name="captcha" id="input-captcha" class="form-control" />
          </div>
        </div>

        <div class="form-group">
            <div class="col-sm-10 pull-right">
	            <img src="index.php?route=tool/captcha" alt="" />

	            <?php if ($error_captcha) { ?>
			        <div class="text-danger"><?php echo $error_captcha; ?></div>
	            <?php } ?>
            </div>
        </div>
				<input type="hidden" name="tree_id" value="<?=$tree_id ? $tree_id : 0?>">
			</fieldset>

			<div class="buttons">
				<div class="pull-right">
					<input id="upload_image_btn" class="btn btn-primary" style="margin-right: 5px;" type="button" value="<?php echo $button_photo; ?>">
					<input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>">
				</div>
			</div>
		</form>

		<?php require(DIR_TEMPLATE.'default/template/common/imageuploader.tpl'); ?>

		<script type="text/javascript">
			var img = document.getElementById('upload_image_img'),
				img_block = document.getElementById('img_block');

			img_block.style.display = img.getAttribute('src') == '' ? 'none' : 'block';
			$("input[name='leedform_phone']").keypress(testPhoneInput);

			function imageUploaded(json) {
				if (json['error']) alert(json['error']);

				if (json['success']) {
					img.src = document.getElementById('input-photo').value = json['img_url'];
					img_block.style.display = 'block';
				}
			}

			addUploadHandler(document.getElementById('upload_image_btn'), imageUploaded);
		</script>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
