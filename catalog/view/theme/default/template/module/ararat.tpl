<div class="list-group">
<?php if ($catalog_id !== 0) $route .= "&amp;path=$catalog_id"; ?>
<?php if ($information_id !== -1) $route .= "&amp;information_id=$information_id"; ?>
<?php foreach ($informations as $information) { ?>
	<?php if (preg_match("|=$route$|", $information['href'])) { ?>
	<a href="<?php echo $information['href']; ?>" class="list-group-item active"><b><?php echo mb_strtoupper($information['title'], 'UTF-8'); ?></b></a>
	<?php } else { ?>
	<a href="<?php echo $information['href']; ?>" class="list-group-item"><?php echo mb_strtoupper($information['title'], 'UTF-8'); ?></a>
	<?php } ?>
<?php } ?>
</div>