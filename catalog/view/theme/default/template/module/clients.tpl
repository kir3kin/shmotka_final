<div class="row">
	<div class="col-xs-12 wrapper">
		<h3><?php echo $heading_title; ?></h3>
		<div class="client-wrapper clearfix">
<?php foreach ($clients_image as $client_image) { ?>
			<div class="client-image">
				<img src="<?php echo $client_image; ?>" alt="" class="img-responsive" />
			</div>
<?php } ?>
		</div>
	</div>
</div>
