<?php
class ModelModuleCategorysales extends Model {
	public function __construct($registry) {
		parent::__construct($registry);
	}

	public function getCategoryLastSpecial($category_id){		$query = $this->db->query("SELECT ps.date_end FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (ps.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (p2c.category_id = c2s.category_id) WHERE p2c.category_id = '" . (int)$category_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  ORDER BY ps.date_end DESC");
		return $query->row;	}

	public function getCategoryCheapest($category_id){
		$query = $this->db->query("SELECT IF(ISNULL(ps.price), p.price, ps.price) as lowerPrice FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_special ps ON (p.product_id = ps.product_id)  LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (p2c.category_id = c2s.category_id) WHERE p2c.category_id = '" . (int)$category_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  ORDER BY lowerPrice ASC");
		return $query->row;
	}

	public function getCategoryBiggestDiscount($category_id){
		$query = $this->db->query("SELECT (p.price-ps.price) as lowerPrice FROM " . DB_PREFIX . "product_special ps LEFT JOIN " . DB_PREFIX . "product p ON (ps.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (ps.product_id = p2c.product_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (p2c.category_id = c2s.category_id) WHERE p2c.category_id = '" . (int)$category_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  ORDER BY lowerPrice DESC");
		return $query->row;
	}

}
