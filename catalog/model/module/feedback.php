<?php
class ModelModuleFeedback extends Model {
	public function add($name, $comment, $photo, $parent_id) {
		if (!empty($parent_id)) {
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "feedback_entries (author, text, photo, parent_id) VALUES ('" . $name . "', '" . $comment . "', '" . $photo . "', '" . (int)$parent_id . "')");
		} else {
			$query = $this->db->query("INSERT INTO " . DB_PREFIX . "feedback_entries (author, text, photo) VALUES ('" . $name . "', '" . $comment . "', '" . $photo . "')");
		}
	}

	public function getApproved() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "feedback_entries WHERE approved=1 ORDER BY timestamp DESC LIMIT 25");
		return $query->rows;
	}
}
