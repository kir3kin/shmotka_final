<?php
class ModelModuleClients extends Model {
	public function getAll() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "our_clients ORDER BY id");
		return $query->rows;
	}
}
