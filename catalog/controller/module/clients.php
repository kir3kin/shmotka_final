<?php
class ControllerModuleClients extends Controller {
	public function index() {
		$this->load->language('module/clients');
		$this->load->model('module/clients');

		$clients_image = $this->model_module_clients->getAll();

		$this->load->model('tool/image');

		foreach ($clients_image as $client_image) {
			if (is_file(DIR_IMAGE . $client_image['image'])) {
				$image = $this->config->get('config_url') . 'image/' . $client_image['image'];
			} else {
				$image = $this->config->get('config_url') . 'image/placeholder.png';
			}
			$data['clients_image'][] = $image;
		}

		$data['heading_title'] = $this->language->get('heading_title');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/clients.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/clients.tpl', $data);
		} else {
			return $this->load->view('default/template/module/clients.tpl', $data);
		}
	}
}