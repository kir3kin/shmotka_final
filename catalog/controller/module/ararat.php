<?php
class ControllerModuleArarat extends Controller {
	public function index() {
		$this->load->language('module/ararat');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['information_id'])) {
			$data['information_id'] = (int)$this->request->get['information_id'];
		} else {
			$data['information_id'] = -1;
		}

		if (isset($this->request->get['route'])) {
			$data['route'] = (string)$this->request->get['route'];
		} else {
			$data['route'] = '';
		}

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['catalog_id'] = (int)$parts[0];
		} else {
			$data['catalog_id'] = 0;
		}

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {

			if (!isset($result['referer_to'])) {
				$href = $this->url->link('information/information', 'information_id=' . $result['information_id']);
			} else {
				$href = $this->url->link($result['referer_to']);
			}

			$data['informations'][] = array(
				'title' => $result['title'],
				'href'  => $href,
				'information_id' => $result['information_id']
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/ararat.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/ararat.tpl', $data);
		} else {
			return $this->load->view('default/template/module/ararat.tpl', $data);
		}
	}
}