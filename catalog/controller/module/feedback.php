<?php
class ControllerModuleFeedback extends Controller {
		public function index() {
			$this->load->model('module/feedback');
			$feedback_entries = $this->model_module_feedback->getApproved();
			$data['feedback_block_header'] = 'Отзывы';
			$data['feedback_entries'] = array();


			function get_elements_data($feedback) {
				return array(
					'author' => $feedback['author'],
					'dt' => date("d.m.Y H:i:s", strtotime($feedback['timestamp'])),
					'text' => $feedback['text'],
					'photo' => $feedback['photo'],
					'id' => $feedback['id']
				);
			}

			function circle_children($childrens, $parents) {//добавить новую переменную содержащую изначальное количество детей а после внутреннего цикла for переопределять её
				//$old_childrens = $childrens;
				for($i = 0; $i < count($parents); $i++) {
					for($j = 0; $j < count($childrens); $j++) {
					// for($j = 0; $j < count($old_childrens); $j++) {
						if (isset($childrens[$j])) {//можно убрать это условие
						// if (isset($old_childrens[$j])) {//можно убрать это условие
							if ($parents[$i]['id'] === $childrens[$j]['parent_id']) {
							// if ($parents[$i]['id'] === $old_childrens[$j]['parent_id']) {
								$parents[$i]['children'][] = get_elements_data($childrens[$j]);
								// $parents[$i]['children'][] = get_elements_data($old_childrens[$j]);//удалять использованные элементы массива детей, не повлияв на работу цикла for по обработке детей
								// unset($childrens[$j]);
							}
						}
					}
					// if (isset($childrens) && !empty($childrens)) {
					if (isset($parents[$i]['children'])) {
						$parents[$i]['children'] = circle_children($childrens, $parents[$i]['children']);
					}
					// }
				}
				return $parents;
			}

			function add_children_data($feedback) {//попробывать соединить circle_children и add_children_data
				foreach ($feedback as $feedback_entry) {
					if (empty($feedback_entry['parent_id'])) {
						$data[] = get_elements_data($feedback_entry);
					} else {
						$childrens[] = $feedback_entry;
					}
				}
				if (!empty($childrens)) {
					$data = circle_children($childrens, $data);
				}
				return $data;
			}

			if (!empty($feedback_entries)) {
				$data['feedback_entries'] = add_children_data($feedback_entries);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/feedback.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/feedback.tpl', $data);
				} else {
				return $this->load->view('default/template/module/feedback.tpl', $data);
			}
		}
}
