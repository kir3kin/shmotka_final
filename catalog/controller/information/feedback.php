<?php
class ControllerInformationFeedback extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('information/feedback');
		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			unset($this->session->data['captcha']);
			$this->load->model('module/feedback');

			$name = $this->request->post['name'];
			$comment = $this->request->post['comment'];
			$photo = $this->request->post['photo'];

			$parent_id = $this->request->post['tree_id'] !== 0 ? $this->request->post['tree_id'] : '';

			$this->model_module_feedback->add($name, $comment, $photo, $parent_id);

			$config_email = $this->config->get('config_email');
			$mail_text = $this->language->get('email_prev_text') . "\n" . strip_tags($comment);

			$mail = new Mail([$config_email]);
			$mail->setTo($config_email);
			$mail->setFrom($config_email);
			$mail->setSender($this->config->get('config_address'));
			$mail->setSubject(sprintf($this->language->get('email_subject'), $this->request->post['name']));
			$mail->setText($mail_text);
			$mail->send();

			$this->response->redirect($this->url->link('information/feedback/success'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/feedback')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['heading_info'] = $this->language->get('heading_info');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_captcha'] = $this->language->get('entry_captcha');

		$data['button_photo'] = $this->language->get('button_photo');
		$data['button_submit'] = $this->language->get('button_submit');

		$data['tree_id'] = 0;

		if (($this->request->server['REQUEST_METHOD'] === 'GET') && isset($this->request->get['id'])) {
			if ($this->validateTree()) {
				$data['tree_id'] = (int) $this->request->get['id'];
			}
			else $data['tree_id'] = -1;
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['comment'])) {
			$data['error_comment'] = $this->error['comment'];
		} else {
			$data['error_comment'] = '';
		}

		if (isset($this->error['captcha'])) {
			$data['error_captcha'] = $this->error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}

		$data['action'] = $this->url->link('information/feedback');

		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} else {
			$data['name'] = $this->customer->getFirstName();
		}

		if (isset($this->request->post['comment'])) {
			$data['comment'] = $this->request->post['comment'];
		} else {
			$data['comment'] = '';
		}

		if (isset($this->request->post['photo'])) {
			$data['photo'] = $this->request->post['photo'];
		} else {
			$data['photo'] = '';
		}

		if (isset($this->request->post['captcha'])) {
			$data['captcha'] = $this->request->post['captcha'];
		} else {
			$data['captcha'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/feedback.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/feedback.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/feedback.tpl', $data));
		}
	}

	public function success() {
		$this->load->language('information/feedback');
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/feedback')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_message'] = $this->language->get('text_success');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	public function error() {
		$this->load->language('information/feedback');
		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/feedback')
		);

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_message'] = $this->language->get('text_error');
		$data['button_continue'] = $this->language->get('button_continue');
		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
		}
	}

	protected function validateTree() {
		$id = $this->request->get['id'];
		if (!is_numeric($id) || ((int) $id <= 0)) return false;
		return true;
	}

	protected function validate() {
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if ((utf8_strlen($this->request->post['comment']) < 10) || (utf8_strlen($this->request->post['comment']) > 3000)) {
			$this->error['comment'] = $this->language->get('error_comment');
		}

		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$this->error['captcha'] = $this->language->get('error_captcha');
		}

		$id = isset($this->request->post['tree_id']) ? $this->request->post['tree_id'] : -1;

		if (!is_numeric($id) || ($id < 0)) {
			$this->response->redirect($this->url->link('information/feedback/error'));
		}

		return !$this->error;
	}
}
