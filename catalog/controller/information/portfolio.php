<?php
class ControllerInformationPortfolio extends Controller {
	public function index() {
		$this->load->language('information/portfolio');
		$this->load->model('module/portfolio');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/portfolio')
		);

        $data['heading_title'] = $this->language->get('heading_title');
        $data['heading_info'] = $this->config->get('portfolio_description');

        $data['portfolio_images'] = array();
        $portfolio_images = $this->model_module_portfolio->get();

        foreach ($portfolio_images as $portfolio_image) {
			$data['portfolio_images'][] = array(
				'id' => $portfolio_image['portfolio_image_id'],
				'path' => $portfolio_image['image'],
				'sort_order' => $portfolio_image['sort_order']
			);
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/portfolio.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/portfolio.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/information/portfolio.tpl', $data));
		}
	}
}
