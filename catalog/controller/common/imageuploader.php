<?php
class ControllerCommonImageUploader extends Controller {
	public function index() {
		echo '';
	}

	public function upload() {
		$this->load->language('common/imageuploader');
		$json = array();

		// Make sure we have the correct directory
		if (isset($this->request->get['directory'])) {
			$get_dir = str_replace(array('../', '..\\', '..'), '', $this->request->get['directory']);
			$directory = rtrim(DIR_IMAGE . 'user/' . $get_dir, '/');
			$url = rtrim(HTTP_SERVER . 'image/user/' . $get_dir, '/');
		} else {
			$directory = DIR_IMAGE . 'user';
			$url = HTTP_SERVER . 'image/user';
		}

		// Check its a directory
		if (!is_dir($directory)) $json['error'] = $this->language->get('error_directory');

		// Process files
		if (!$json) {
			if (!empty($this->request->files['file']['name']) && is_file($this->request->files['file']['tmp_name'])) {
				// Sanitize the filename
				$filename = basename(html_entity_decode($this->request->files['file']['name'], ENT_QUOTES, 'UTF-8'));

				// Validate the filename length
				if ((utf8_strlen($filename) < 3) || (utf8_strlen($filename) > 255)) {
					$json['error'] = $this->language->get('error_filename');
				}

				// Allowed file extension types
				$allowed = array('jpg', 'jpeg', 'gif', 'png');

				if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Allowed file mime types
				$allowed = array('image/jpeg', 'image/pjpeg', 'image/png', 'image/x-png', 'image/gif');

				if (!in_array($this->request->files['file']['type'], $allowed)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Check to see if any PHP files are trying to be uploaded
				$content = file_get_contents($this->request->files['file']['tmp_name']);

				if (preg_match('/\<\?php/i', $content)) {
					$json['error'] = $this->language->get('error_filetype');
				}

				// Return any upload error
				if ($this->request->files['file']['error'] != UPLOAD_ERR_OK) {
					$json['error'] = $this->language->get('error_upload_' . $this->request->files['file']['error']);
				}
			}

			else $json['error'] = $this->language->get('error_upload');
		}

		if (!$json) {
			$filename = time() . '_' . $filename;
			move_uploaded_file($this->request->files['file']['tmp_name'], $directory . '/' . $filename);

			$json['success'] = $this->language->get('text_uploaded');
			$json['img_url'] = $url . '/' . $filename;
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
